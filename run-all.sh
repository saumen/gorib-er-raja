#!/usr/bin/bash

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

set -eu

source "${SCRIPT_DIR}/setup-env.sh"
source "${SCRIPT_DIR}/download.sh"
python "${SCRIPT_DIR}/orders.py"
