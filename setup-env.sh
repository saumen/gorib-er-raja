#!/usr/bin/bash

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

set -eu

virtualenv gorib
source gorib/bin/activate
pip install -r requirements.txt
