# in case of KeyError add the missing key in instrumentToSymbol
# Example: https://api.robinhood.com/instruments/?ids=6bc8e1d6-df72-4d48-911f-0e05a9bdc07f

instrumentToSymbol = {
    "08b76305-89e9-4566-a9d3-c73f0fb4533d": "EA",
    "18c7a7ad-3527-47b4-84b4-ce4db0106531": "SBUX",
    "1be547f6-699e-4515-a0b3-f4c084aa0082": "TGT",
    "1db76c1d-74b6-46a3-a1e5-c58ab489d5c2": "CMCSA",
    "279d787b-515b-4f9c-a684-45f92afb557f": "ROKU",
    "2b456f6a-3287-4757-abf9-327383d2c708": "T",
    "2ed64ef4-2c1a-44d6-832d-1be84741dc41": "DIS",
    "2fb13c63-fff5-4fab-9d17-8c84da973bbf": "SNE",
    "35875944-ffb7-47eb-a2e5-582ba9f26a8d": "ZM",
    "450dfc6d-5510-4d40-abfb-f633b7d9be3e": "AAPL",
    "4ae77f4e-cea3-4d25-9154-e56ff5f910cc": "WORK",
    "500e9be2-9e0e-4e1d-a6a0-5db42afb7c6e": "M",  # Macy's,
    "50810c35-d215-4866-9758-0ada4ac79ffa": "MSFT",
    "5d61146c-5e00-4e04-836f-239d359b9602": "LOGI",
    "5ea5e761-1747-4911-beec-5a24af338329": "QCOM",
    "63893a46-50b6-43db-88ce-10d7340a0c69": "PTON",
    "64f09220-e3de-4c4a-bd1d-1d2303935b0c": "ZEN",  # ZenDesk
    "6bc8e1d6-df72-4d48-911f-0e05a9bdc07f": "PD",  # Pager Duty
    "6df56bd0-0bf2-44ab-8875-f94fd8526942": "F",
    "81733743-965a-4d93-b87a-6973cb9efd34": "NFLX",
    "93ec8a9c-d587-4867-9996-67733ec86980": "ATVI",
    "940fc3f5-1db5-4fed-b452-f3a2e4562b5f": "AMD",
    "ad059c69-0c1c-4c6b-8322-f53f1bbd69d4": "INTC",  # Intel
    "ad1edbfb-19c8-4939-be66-1f09bd65b902": "WMT",  # Walmart
    "aec9d597-9fdb-4393-af8b-da2019e2c179": "NKE",  # Nike
    "b2e06903-5c44-46a4-bd42-2a696f9d68e1": "BABA",
    "d3aa8b83-b8a3-4a86-acb2-0c22061cd7e8": "MMM",
    "e39605bf-9789-41f5-8b50-9bd38fec8f17": "ZNGA",
    "ebab2398-028d-4939-9f1d-13bf38f81c50": "FB",
    "f87d7cd7-a842-47cc-9b32-c607d96e7dfb": "PCG",
}


def instrument(uuid):
    return instrumentToSymbol[uuid]


def stripURL(instruments):
    return instruments.map(lambda i: i
                           .replace("https://api.robinhood.com/instruments/", "")
                           .replace('/', "")
                           ).map(lambda i: instrumentToSymbol[i])
