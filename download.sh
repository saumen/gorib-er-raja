#!/usr/bin/bash

# Downloads orders from Robinhood

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

set -eu

ACCESS_TOKEN_SOURCE="${SCRIPT_DIR}/.rh_access_token"

if [[ ! -f ${ACCESS_TOKEN_SOURCE} ]]; then
  echo "Missing access token source for Robinhood"
  exit 1
fi

ACCESS_TOKEN=`cat ${ACCESS_TOKEN_SOURCE}`
echo "Loaded robinhood access token"

downloadJSON() {
  ENDPOINT=$1
  SAVE_AS=$2

  echo "Downloading ${ENDPOINT} from Robinhood in ${SAVE_AS} ..."
  curl "https://api.robinhood.com/${ENDPOINT}" \
    -H 'authority: api.robinhood.com' \
    -H 'x-robinhood-api-version: 1.315.0' \
    -H 'sec-fetch-dest: empty' \
    -H "authorization: ${ACCESS_TOKEN}" \
    -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36' \
    -H 'accept: */*' \
    -H 'origin: https://robinhood.com' \
    -H 'sec-fetch-site: same-site' \
    -H 'sec-fetch-mode: cors' \
    -H 'referer: https://robinhood.com/' \
    -H 'accept-language: en-US,en;q=0.9,bn;q=0.8' \
    --progress-bar \
    --compressed > "${SAVE_AS}"
}

OUTPUT_DIR="${SCRIPT_DIR}/out/imports"
if [[ ! -d ${OUTPUT_DIR} ]]; then
  mkdir -p "${OUTPUT_DIR}"
fi

ORDERS_FILE="${OUTPUT_DIR}/orders.json"
downloadJSON "orders/?page_size=10000" "${ORDERS_FILE}"

# validate
ls -lh $OUTPUT_DIR

file_size=$(wc -c "${ORDERS_FILE}" | awk '{print $1}')
if [[ $file_size -lt 70 ]]; then
  echo "Something went wrong. Check the ${ORDERS_FILE} for details."
  exit 1
else
  echo "validated downloaded file"
fi
