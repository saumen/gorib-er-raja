# The Elevator Pitch
Did you ever find yourself lost in orders history in Robinhood among various order states?
Did you ever wish to only see pending orders across various stock instruments with zero mouse click?
Well keep looking there but when you get a chance don't forget to try out this `gorib-er-raja` tool.

With `gorib-er-raja` you can securely navigate and analyze your orders.
With your super coding skill you can customize the scripts and enhance according to your preference.

# TL;DR Manual
## Requirements
- pip
- python 3.8.5
- python-virualenv

## Inject access token
In order to download your data from `Robinhood`, the `download.sh` script requires your access
token. The token is issued when you login on the website and it expires after a certain time.

To obtain the access token 

- Use the developer tool/Network on your web browser
- Look for URLs starting with `api.robinhood.com`
- Under `Headers`/`Request Headers` look for key `Authorization` 
- Copy the value including `Bearer`. Paste the entire value in a text file `.rh_access_token`.

Example:
```bash
❯ cat .rh_access_token  
Bearer %encoded value%
```

Now run the script aggregator:
```bash
❯ ./run-all.sh 
```

The script will setup virual python env, install dependencies, download your orders and analyze.

If you want to run any individual step (for example: only analzer script) see: `Detail Manual`.

# Troubleshoot

> KeyError: '%UUID%

Most likely your transactions include an instrument which is not defined in `modules/instruments.py`
map. See the instructions on the file and update the map. Feel free to submit a PR.

# Detail Manual
## Setup runtime environment
```bash
./setup-env.sh
```


## Download
```bash
gorib ❯ ./download.sh 
```

## Run
```bash
gorib ❯ python orders.py
```
