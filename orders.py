import os.path

import numpy as np
import pandas as pd

import modules.instruments as instruments

ordersSource = "out/imports/orders.json"

columns = [
    'symbol',
    'average_price',
    'price',
    'stop_price',
    'quantity',
    'cumulative_quantity',
    'side',
    'fees',
    'state',
    'last_transaction_at',
    'instrument'
]

reportColumns = [
    'last_transaction_at',
    'symbol',
    'side',
    'state',
    'price',
    'stop_price',
    'average_price',
    'quantity'
]


def select(statusIn, columns, transactions):
    return transactions[transactions.state.isin(statusIn)][columns]


if os.path.isfile(ordersSource):
    rhJson = pd.read_json(ordersSource).results
    rhDf = pd.DataFrame.from_records(rhJson, columns=columns)

    rhDf['symbol'] = instruments.stripURL(rhDf.instrument)

    quantityColumns = ['cumulative_quantity', 'quantity']
    rhDf[quantityColumns] = rhDf[quantityColumns].applymap(lambda x: int(float(x)))

    amountColumns = ['average_price', 'fees', 'price', 'stop_price']
    rhDf[amountColumns] = rhDf[amountColumns].fillna("0").applymap(lambda x: float(x))

    states = rhDf.state.unique()
    print('states in your orders:', states)

    pendingTransactions = select(['confirmed'], reportColumns, rhDf)
    print('Pending Transactions:\n', pendingTransactions, '\n')

    queuedTransactions = select(['queued'], reportColumns, rhDf)
    print('Queud Transactions:\n', queuedTransactions, '\n')

    cancelledTransactions = select(['cancelled'], reportColumns, rhDf)
    recentlyCancelled = cancelledTransactions\
        .head(20)\
        .sort_values(['last_transaction_at', 'side'], ascending=False)

    print('Recently Cancelled:\n', recentlyCancelled, '\n')

else:
    print("Missing orders source file '%s'. Please run download.sh" % ordersSource)
